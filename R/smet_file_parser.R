################################################################################
# Copyright 2019 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
#A SMET meteorological data file parser class.
#Michael Reisecker, 2019-02

########################################
#  SMET CLASS DEFINITION               #
########################################

#' @rdname read.smet-SMET-method
setGeneric("read.smet", function(object, smetfile) {
	standardGeneric("read.smet")
})

#' @rdname write.smet-SMET-method
setGeneric("write.smet", function(object, smetfile = NA_character_) {
	standardGeneric("write.smet")
})

#' @rdname get.raw.smet-SMET-method
setGeneric("get.raw.smet", function(object, smetfile) {
	standardGeneric("get.raw.smet")
})

#' @rdname convert.units-SMET-method
setGeneric("convert.units", function(object) {
	standardGeneric("convert.units")
})

#' @rdname between-SMET-method
setGeneric("between", function(object, sdate = NA_character_, edate = NA_character_) {
	standardGeneric("between")
})

#' @rdname get.header.val-SMET-method
setGeneric("get.header.val", function(object, key, vectorize = FALSE) {
	standardGeneric("get.header.val")
})

#' @rdname set.header.val-SMET-method
setGeneric("set.header.val", function(object, key, value) {
	standardGeneric("set.header.val")
})

#' @rdname update.fields-SMET-method
setGeneric("update.fields", function(object) {
	standardGeneric("update.fields")
})

#' @rdname set.station_id-SMET-method
setGeneric("set.station_id", function(object, statid) {
	standardGeneric("set.station_id")
})

#' @rdname set.smetfile-SMET-method
setGeneric("set.smetfile", function(object, smetfile) {
	standardGeneric("set.smetfile")
})

#' @rdname set.header-SMET-method
setGeneric("set.header", function(object, header) {
	standardGeneric("set.header")
})

#' @rdname get.timespan-SMET-method
setGeneric("get.timespan", function(object) {
	standardGeneric("get.timespan")
})

#' A SMET file parser class.
#' @description
#' This class is a light-weight parser for the SMET meteorological data format.
#' @details
#' SMET specifications:
#' https://models.slf.ch/docserver/meteoio/SMET_specifications.pdf
#' @slot smetfile Path to SMET file to parse.
#' @slot header List of SMET header entries.
#' @slot data Meteorological dataset.
#' @slot time Timestamps for the dataset.
#' @slot fields List of present parameters.
#' @slot station_id Identifier for the weather station.
#' @examples
#' \dontrun{
#' smet <- SMET("/path/to/smetfile.smet")
#' first.temp <- smet@data$TA[[1]]
#' timespan <- get.timespan(smet)
#' statname <- get.header.val(smet, "station_name")}
#' @importFrom methods new
#' @export SMET
setClass("SMET", slots = list(smetfile = "character", header = "list", data = "data.frame",
    time = "POSIXlt", fields = "character", station_id = "character")) -> SMET

#' This call tells the \code{as()} function how to convert from a data.frame.
#' Dates are expected in the first column of the data.frame, followed by
#' arbitrary data columns. Meteo fields are set to the data.frame's column names.
#' The station_id and header fields must be set separately.
#' @name as.smet
setAs("data.frame", "SMET",
	function(from) {
		outsmet <- SMET()
		subs <- gsub("T", " ", from[,1], fixed = T)
		outsmet@time <- as.POSIXlt(subs)
		outsmet@data <- from[,-1]
		outsmet@fields <- colnames(from)
		return(outsmet)
	}
)

#' Extract the SMET object's data.frame container. Note that this does not
#' include the dates column, this must be queried separately.
#' @name as.data.frame
setAs("SMET", "data.frame",
	function(from) {
		outdf <- from@data
		colnames(outdf) <- from@fields[-1]
		return(outdf)
	}
)

########################################
#  SMET CLASS INITIALIZATION           #
########################################

#' Initialize the SMET file parser.
#' @description
#' Initializer for a SMET parser object. Opens the specified SMET file.
#' @param .Object This SMET parser.
#' @param smetfile SMET file to open.
#' @return Initialized SMET file parser.
#' @keywords internal
setMethod("initialize", signature = "SMET",
	definition = function(.Object, smetfile = NA_character_) {
		.Object@station_id <- NA_character_
		if (!is.na(smetfile))
			.Object <- read.smet(.Object, smetfile)
	  	return(.Object)
	}
) #end setMethod

########################################
#  SMET CLASS MEMBER FUNCTIONS         #
########################################

#' Parse a SMET file.
#' @description
#' This function stores a SMET file's contents into internal containers.
#' @param object This SMET parser object.
#' @param smetfile Path to SMET file to parse.
#' @return SMET object with parsed file contents.
#' @export
setMethod("read.smet", signature = "SMET",
	function(object, smetfile) { #read and process a smet file
		raw <- get.raw.smet(object, smetfile)

		object@header <- raw[[1]]
		object@data <- as.data.frame(raw[[2]])

		object@station_id <- get.header.val(object, "station_id")
		object@fields <- get.header.val(object, "fields", vectorize = T)
		if (!is.null(object@fields))
			colnames(object@data) <- object@fields

		#Convert time strings to time list and save to dedicated slot. For this we
		#replace the ISO standard with R standard so that a few formats are automatically
		#recognized (e. g. both 00:15 and 00:15:30 would be time stamps):
		subs <- gsub("T", " ", object@data$timestamp, fixed = T)
		object@time <- as.POSIXlt(subs)
		object@data$timestamp <- NULL #remove timestamp from dataframe
		object <- convert.units(object) #apply header's conversion info

		return(object)
	}
) #end setMethod

#' Write out a SMET file to the file system.
#' @description
#' This function stores a SMET object's contents in SMET file format.
#' @details
#' Depending on the SMET's data source, the user is responsible to set all
#' necessary fields beforehand.
#' @param object This SMET parser object.
#' @param smetfile Path to write to. If omitted, the currently set smetfile will be used.
#' @importFrom utils write.table
#' @export
setMethod("write.smet", signature = "SMET",
	function(object, smetfile) { #read and process a smet file
		if (is.na(smetfile))
			smetfile <- object@smetfile
		header <- paste(paste(names(object@header), "=", object@header), collapse = "\n")
		cat("SMET 1.1 ASCII\n[HEADER]\n", file = smetfile)
		cat(paste0(header, "\n"), file = smetfile, append = T)
		cat(paste("fields =", paste(object@fields, collapse = "\t"), "\n"), file = smetfile, append = T)
		cat("\n[DATA]\n", file = smetfile, append = T)
		dataset <- cbind(strftime(object@time, format = "%Y-%m-%dT%H:%M:%S"), object@data)
		write.table(dataset, file = smetfile, sep = "\t",
		    row.names = F, col.names = F, quote = F, append = T)
	}
) #end setMethod

#' Read raw text contents of a SMET file.
#' @description
#' This function reads in text data from the file system.
#' @details
#' It is assumed that there are no more than 100 header lines. The file will be reported as
#' corrupt if this limit is exceeded.
#' @param object This SMET parser object.
#' @param smetfile Path to SMET file to read.
#' @return List with header and data text blocks as items.
#' @importFrom utils read.csv read.table
#' @keywords internal
setMethod("get.raw.smet", signature = "SMET",
	function(object, smetfile) { #read header and data lines from file system
		idx.datasection <- grep("[DATA]", readLines(smetfile, n = 100), fixed = T) #return index of line "[DATA]"
		data <- read.csv(file = smetfile, sep = "", skip = idx.datasection, head = F,
		    na.strings = "-999", stringsAsFactors = F, strip.white = T)

		idx.headersection <- grep("[HEADER]", readLines(smetfile, n = idx.datasection), fixed = T) #return index of line "[HEADER]"
		header <- read.table(file = smetfile, sep = "=", skip = idx.headersection,
		    nrows = idx.datasection - idx.headersection - 1, stringsAsFactors = F, strip.white = T, quote = NULL)
		header.values <- as.list(header[,2])
		names(header.values) <- header[,1]
		return(list(header.values, data))
	}
) #end setMethod

#' Convert units in SMET according to SMET header.
#' @description
#' This function looks for multipliers and offsets in the SMET header and
#' if found applies those to the data.
#' @param object This SMET parser object.
#' @return SMET parser with all data in SI units.
#' @keywords internal
setMethod("convert.units", signature = "SMET",
	function(object) {
		unit.multiplier <- get.header.val(object, "units_multiplier", vectorize = T)
		if (!is.null(unit.multiplier)) {
			unit.multiplier <- as.numeric(unit.multiplier[-1]) #remove time
			object@data <- sweep(object@data, 2, unit.multiplier, "*")
		}
		unit.offset <- get.header.val(object, "units_offset", vectorize = T)
		if (!is.null(unit.offset)) {
			unit.offset <- as.numeric(unit.offset[-1]) #remove time
			object@data <- sweep(object@data, 2, unit.offset, "+")
		}
		return(object)
	}
) #end setMethod

#' Cut SMET file to a timespan.
#' @description
#' This function keeps all entries between two dates and throws away the rest.
#' @details
#' Multiple time formats are recognized, e. g. \code{yyyy-mm-ddTHH:MM:SS} and \code{yyyy-mm-dd HH:MM}.
#' @param object This SMET parser object.
#' @param sdate Start date and time of desired interval.
#' @param edate End date and time of desired interval.
#' @return SMET file object cut to time interval.
#' @export
setMethod("between", signature = "SMET",
	function(object, sdate, edate) {
		idx <- rep(TRUE, length(object@time))
		if (!is.na(sdate)) {
			sdate <- as.POSIXlt(gsub("T", " ", sdate, fixed = T)) #allow conversion to recognize more formats
			idx[object@time < sdate] <- FALSE
		}
		if (!is.na(edate)) {
			edate <- as.POSIXlt(gsub("T", " ", edate, fixed = T))
			idx[object@time > edate] <- FALSE
		}
		object@data <- object@data[idx,]
		object@time <- object@time[idx,]
		return(object)
	}
) #end setMethod

#' Retrieve value of a SMET header field.
#' @description
#' This function looks up a value in the SMET file header.
#' @param object This SMET parser object.
#' @param key Name of header field to extract.
#' @param vectorize Split value into a vector at whitespaces?
#' @return Value of a header field (numeric if it can be coerced).
#' @export
setMethod("get.header.val", signature = "SMET",
	function(object, key, vectorize) { #find a key in the header and return its value ("key = value")
		idx <- match(key, names(object@header))
		val <- object@header[[idx]]
		if (is.null(val))
			return(NULL)
		if (vectorize & !is.null(val)) { #split and return as vector
			val <- unlist(strsplit(val, "\\s+"))
		} else { #try to auto-cast single values
			try.cast <- suppressWarnings(as.numeric(val))
			if (!is.na(try.cast))
				val <- try.cast
		}
		return(val)
	}
) #end setMethod

#' Set a value of the SMET header field.
#' @description
#' This function sets a specific header field in the SMET description.
#' @param object This SMET parser object.
#' @param key Name of header field to set.
#' @param value Value of the header field to set.
#' @return SMET object with the new header value set.
#' @export
setMethod("set.header.val", signature = "SMET",
	function(object, key, value) {
		object@header[key] <- value
		object <- update.fields(object)
		return(object)
	}
) #end setMethod

#' After some header fields have been set, update specific slots.
#' @description
#' If fields are changed for example in \code{set.header.vals()} this function updates
#' fixed slots like the station_id and fields.
#' @param object This SMET parser object.
#' @return SMET parser with updated fields.
#' @keywords internal
setMethod("update.fields", signature = "SMET",
	function(object) {
		id_idx <- match("station_id", names(object@header)) #auto-set station_id if available
		if (!is.na(id_idx))
			object@station_id <- object@header[[id_idx]]
		fields_idx <- match("fields", names(object@header)) #auto-set fields, but keep old ones if n/a
		if (!is.na(fields_idx))
			object@fields <- get.header.val(object, "fields", vectorize = T)
		return(object)
	}
) #end setMethod

#' Set the underlying INI file.
#' @description
#' If a SMET object has been created without a file, this function sets the file name
#' ini retrospect (for example after coercing from a data.frame).
#' @param object This SMET parser object.
#' @param smetfile Path to ini file on file system.
#' @return SMET object with the ini file location set.
#' @export
setMethod("set.smetfile", signature = "SMET",
	function(object, smetfile) {
		object@smetfile <- smetfile
		return(object)
	}
) #end setMethod

#' Set the SMET station ID.
#' @description
#' This function sets the SMET object's station_id field.
#' @param object This SMET parser object.
#' @param statid The station's identification string.
#' @return SMET object with the station ID set.
#' @export
setMethod("set.station_id", signature = "SMET",
	function(object, statid) {
		object@station_id <- statid
		object@header[["station_id"]] <- statid
		return(object)
	}
) #end setMethod

#' Set the SMET header fields.
#' @description
#' This function sets the SMET object's header. This may be necessary for example after
#' coercing from a data.frame with \code{as(df, "SMET")}.
#' @param object This SMET parser object.
#' @param header List of header fields (name/value pairs).
#' @return Value of a header field (numeric if it can be coerced).
#' @examples
#' \dontrun{
#' mm <- c("2019-04-15T00:00:00", "2019-04-15T00:00:00", "270", "1.5", "273", "1.6")
#' dim(mm) <- c(2, 3)
#' df <- as.data.frame(mm)
#' colnames(mm) <- c("timestamp", "TA", "HS")
#' smet <- as(df, "SMET")
#' smet <- set.header(smet, list("station_id" = "ISAALBAC6", altitude = "1040"))}
#' @export
setMethod("set.header", signature = "SMET",
	function(object, header) {
		object@header <- header
		object <- update.fields(object)
		return(object)
	}
) #end setMethod

#' Calculate time span of dataset.
#' @description
#' This function returns the time difference between the first and last
#' data points in the dataset.
#' @param object This SMET parser object.
#' @return Time span in days.
#' @export
setMethod("get.timespan", signature = "SMET",
	function(object) { #return difference in days:
		return(difftime(object@time[length(object@time)], object@time[1], units = "days"))
	}
) #end setMethod

